package model;

import java.util.Date;

public interface Person {

	String getName();
	
	String getLastname();
	
	Date getBirthDate();
	
}
