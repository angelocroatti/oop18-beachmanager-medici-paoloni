package model;

import java.util.Date;

public interface Client extends Person{
	
	String getId();
	
	Date getRegistrationDate();

}
