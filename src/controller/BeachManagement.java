package controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;
import java.util.stream.IntStream;

import model.Client;
import model.ClientImpl;
import model.Employee;
import model.SunBench;
import model.Umbrella;
import utils.Pair;
import utils.Position;
import utils.constants.Shift;
import view.employee.EmployeeManagementInterface;

public class BeachManagement implements BeachManagerManagement {

	Map<Map<Umbrella, List<SunBench>>, Client> umbrellaClient;

	Map<Date, Map<Shift, Employee>> employesShift;

	private UserFactory userFactory;

	private EmployeeManagementInterface emi;

	public BeachManagement(UserFactory userFactory) {
		this.umbrellaClient = new HashMap<>();
		this.employesShift = new HashMap<>();
		this.userFactory = userFactory;

		this.populateMaps();
	}

	@Override
	public Client getUmbrellaInfo(Position pos) {
		Client client = null;
		for (Entry<Map<Umbrella, List<SunBench>>, Client> entry : this.umbrellaClient.entrySet()) {
			for (Entry<Umbrella, List<SunBench>> umbrellaEntry : entry.getKey().entrySet()) {
				if (umbrellaEntry.getKey().getPosition().equals(pos)) {
					client = entry.getValue();
				}
			}
		}
		return client;
	}

	@Override
	public int getCotNumber(Position pos) {
		for (Map<Umbrella, List<SunBench>> map : umbrellaClient.keySet()) {
			for (Umbrella umb : map.keySet()) {
				if (umb.getPosition().equals(pos)) {
					return map.get(umb).size();
				}
			}
		}
		return 0;
	}

	@Override
	public double rentEquipment(int numUmbrellas, int numSunBench, Client client) {
		double total = 0.0;
		int umbrellaFree = 0;

		for (Entry<Map<Umbrella, List<SunBench>>, Client> entry : this.umbrellaClient.entrySet()) {
			for (Entry<Umbrella, List<SunBench>> umbrellaEntry : entry.getKey().entrySet()) {
				if (!umbrellaEntry.getKey().isAlreadyOccupied()) {
					umbrellaFree++;
				}
			}
		}

		if (umbrellaFree < numUmbrellas) {
			return 0.0;
		} else {
			for (Entry<Map<Umbrella, List<SunBench>>, Client> entry : this.umbrellaClient.entrySet()) {
				for (Entry<Umbrella, List<SunBench>> umbrellaEntry : entry.getKey().entrySet()) {
					Umbrella umbrella = umbrellaEntry.getKey();
					if (!umbrella.isAlreadyOccupied() && numUmbrellas > 0) {
						total += umbrella.getCost();
						for (int j = 0; j < numSunBench; j++) {
							SunBench sunBench = new SunBench(umbrella.getPosition());
							umbrellaEntry.getValue().add(sunBench);
							total += sunBench.getCost();
						}
						numSunBench = 0;
						entry.setValue(client);
						umbrellaEntry.getKey().rent();
						numUmbrellas--;
					}
				}
			}
		}

		return total;
	}

	@Override
	public void giveBack(Position pos) {
		// TODO Auto-generated method stub
		Iterator<Map.Entry<Map<Umbrella, List<SunBench>>, Client>> it = this.umbrellaClient.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<Map<Umbrella, List<SunBench>>, Client> entry = it.next();
			Map<Umbrella, List<SunBench>> map = entry.getKey();
			Iterator<Map.Entry<Umbrella, List<SunBench>>> mapIterator = map.entrySet().iterator();

			while (mapIterator.hasNext()) {
				Position umbrellaPos = mapIterator.next().getKey().getPosition();
				if (umbrellaPos.equals(pos)) {
					it.remove();
				}
			}
		}
	}

	public void setShift(Employee employee, Shift shift, Date date) {
		Date[] dates = this.employesShift.keySet().toArray(new Date[this.employesShift.size()]);

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int turnDay = cal.get(Calendar.DAY_OF_MONTH);
		int turnMonth = cal.get(Calendar.MONTH);

		for (int i = 0; i < dates.length; i++) {
			cal.setTime(dates[i]);
			int mapDay = cal.get(Calendar.DAY_OF_MONTH);
			int mapMonth = cal.get(Calendar.MONTH);

			if (mapDay == turnDay && turnMonth == mapMonth) {
				Map<Shift, Employee> shiftMap = this.employesShift.get(dates[i]);
				shiftMap.put(shift, employee);
			}
		}

	}

	@Override
	public Pair<Employee, Employee> getShiftInfo(Date date) {
		// TODO Auto-generated method stub
		Pair<Employee, Employee> pair = null;

		Date[] dates = this.employesShift.keySet().toArray(new Date[this.employesShift.size()]);

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int turnDay = cal.get(Calendar.DAY_OF_MONTH);
		int turnMonth = cal.get(Calendar.MONTH);

		for (int i = 0; i < dates.length; i++) {
			cal.setTime(dates[i]);
			int mapDay = cal.get(Calendar.DAY_OF_MONTH);
			int mapMonth = cal.get(Calendar.MONTH);

			if (mapDay == turnDay && turnMonth == mapMonth) {
				Map<Shift, Employee> shiftMap = this.employesShift.get(dates[i]);
				pair = new Pair<Employee, Employee>(shiftMap.get(Shift.MORNING), shiftMap.get(Shift.AFTERNOON));
			}
		}

		return pair;
	}

	@Override
	public boolean isUmbrellaAlreadyRent(Position pos) {
		for (Map<Umbrella, List<SunBench>> map : this.umbrellaClient.keySet()) {
			for (Umbrella umb : map.keySet()) {
				if (umb.getPosition().equals(pos)) {

					if (!umb.isAlreadyOccupied()) {
						return false;
					} else {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void setEmployeeInterface(EmployeeManagementInterface emi) {
		this.emi = emi;
	}

	@Override
	public void removeEmployee(Employee employee) {
		this.userFactory.removeEmployee(employee);
		this.emi.updateEmployeeList(this.userFactory.getAllEmployes());
		for (Map<Shift, Employee> map : this.employesShift.values()) {
			if (map.containsValue(employee)) {
				this.employesShift.remove(this.employesShift.get(map));
				map.clear();
			}

		}
	}

	@Override
	public void addEmployee(String name, String surname, Date birthdate) {
		this.userFactory.addEmployee(name, surname, birthdate);
		this.emi.updateEmployeeList(this.userFactory.getAllEmployes());
	}

	private void populateMaps() {
		String[] names = { "Alessandro", "Giovanni", "Roberto", "Maria Chiara", "Norberto", "Vittoria" };
		String[] surnames = { "Rossi", "Bianchi", "Verdi", "Medici", "Paoloni", "Ciriello" };

		Random r = new Random();

		this.populateUmbrellaClientMap(names, surnames, r);
		this.populateShiftClientMap(names, surnames, r);

	}

	private void populateShiftClientMap(String[] names, String[] surnames, Random r) {

		int birthDay = r.nextInt(32) + 1;
		int birthMonth = r.nextInt(12) + 1;
		int birthYear = r.nextInt(30 + 1) + 1970;

		Date birthdate = new GregorianCalendar(birthYear, birthMonth, birthDay).getTime();

		Employee[] employeeList = this.userFactory.getAllEmployes();

		Calendar cal = new GregorianCalendar();

		for (int i = 0; i < 50; i++) {
			Date date = cal.getTime();
			Map<Shift, Employee> shiftMap = new TreeMap<Shift, Employee>();

			shiftMap.put(Shift.MORNING, employeeList[r.nextInt(employeeList.length)]);
			shiftMap.put(Shift.AFTERNOON, employeeList[r.nextInt(employeeList.length)]);

			this.employesShift.put(date, shiftMap);
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
	}

	
	private void populateUmbrellaClientMap(String[] names, String[] surnames, Random r) {
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 8; j++) {
				int prob = r.nextInt(2);

				if (prob == 1) {
					int row = i;
					int col = j;

					Map<Umbrella, List<SunBench>> umbrellaMap = new HashMap<Umbrella, List<SunBench>>();

					List<SunBench> list = new ArrayList<SunBench>();
					int numSunBench = r.nextInt(4 + 1);

					IntStream.range(0, numSunBench).forEach(e -> {
						list.add(this.generateSunBench(row, col));
					});

					umbrellaMap.put(this.generateUmbrella(i, j, true), list);
					this.umbrellaClient.put(umbrellaMap, this.generateClient(names, surnames, r));
				} else {
					int row = i;
					int col = j;

					Map<Umbrella, List<SunBench>> umbrellaMap = new HashMap<Umbrella, List<SunBench>>();

					List<SunBench> list = new ArrayList<SunBench>();
					umbrellaMap.put(this.generateUmbrella(row, col, false), list);
					this.umbrellaClient.put(umbrellaMap, null);
				}
			}
		}
	}

	private SunBench generateSunBench(int i, int j) {
		return new SunBench(new Position(i, j));
	}

	private Umbrella generateUmbrella(int i, int j, boolean rent) {
		Umbrella umb = new Umbrella(new Position(i, j));
		if (rent) {
			umb.rent();
		}
		return umb;
	}

	private ClientImpl generateClient(String[] names, String[] surnames, Random r) {
		String name = names[r.nextInt(names.length)];
		String surname = surnames[r.nextInt(surnames.length)];

		int birthDay = r.nextInt(32) + 1;
		int birthMonth = r.nextInt(12) + 1;
		int birthYear = r.nextInt(30 + 1) + 1970;

		Date birthdate = new GregorianCalendar(birthYear, birthMonth, birthDay).getTime();
		return new ClientImpl(name, surname, birthdate, new Date());

	}
}
