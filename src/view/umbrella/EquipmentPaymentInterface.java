package view.umbrella;

/**
 * Interfaccia per le operazioni tra Controller e View
 * 
 * @author Samuele Medici, samuele.medici2@studio.unibo.it (Mat. 0000718877 )
 * 
 */
public interface EquipmentPaymentInterface {
	
	/**
	 * 
	 * @return Costo totale dell'attrezzatura
	 */
	double setTotalAmount();

}
