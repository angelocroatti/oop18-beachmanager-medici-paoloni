package view.umbrella;

/**
 * 
 * @author Samuele Medici, samuele.medici2@studio.unibo.it ( Mat. 0000718877 )
 * Interfaccia per la View Umbrella
 *
 */
public interface UmbrellaInterface {

	/**
	 * Apre il pannello per la gestione delle prenotazioni 
	 */
	void openBookingPanel();
	
	
}
