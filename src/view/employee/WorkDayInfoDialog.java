package view.employee;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.BeachManagement;
import model.Employee;
import utils.Pair;
import view.InfoDialog;

/**
 * InfoDialog per i lavoratori che hanno il turno in un determinato giorno
 * 
 * @author Samuele Medici, samuele.medici2@studio.unibo.it (Mat. 0000718877 )
 *
 */
public class WorkDayInfoDialog extends InfoDialog {

	private static final long serialVersionUID = 2061744525583325239L;

	/**
	 * 
	 * @param parent        Frame padre
	 * @param selectedDay   giorno selezionato
	 * @param selectedMonth mese selezionato
	 * @param selectedYear  anno selezionato
	 * @param management    controller per ottenere informazioni
	 */
	public WorkDayInfoDialog(Frame parent, int selectedDay, int selectedMonth, int selectedYear,
			BeachManagement management) {
		super(parent);

		Date date = new GregorianCalendar(selectedYear, selectedMonth, selectedDay).getTime();

		System.out.println(date.toString());

		// Pannello centrale
		JPanel mainPanel = new JPanel(new GridBagLayout());

		Pair<Employee, Employee> employees = management.getShiftInfo(date);

		// Se la coppia è nulla, vuol dire che non sono stati assegnati i turni
		if (employees != null) {

			// Controllo se esistono i dipendenti per quel turno! 
			if (employees.getFirst() != null) {
				JPanel morningShift = new JPanel();
				JLabel morningLabel = new JLabel("Turno mattina: " + employees.getFirst().getFullName());
				morningShift.add(morningLabel);
				mainPanel.add(morningShift, new GridBagConstraints());
			}

			if (employees.getSecond() != null) {
				JPanel afternoonShift = new JPanel();
				JLabel afternoonLabel = new JLabel("Turno pomeriggio: " + employees.getSecond().getFullName());
				afternoonShift.add(afternoonLabel);
				mainPanel.add(afternoonShift);
			}

		} else {
			
			int currentDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
			int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
			// Se sono giorni passati mostro una label con un messaggio diverso
			if (currentDay < selectedDay && currentMonth <= selectedMonth) {
				mainPanel.add(new JLabel("Giorno già passato!"), new GridBagConstraints());
			} else {
				mainPanel.add(new JLabel("Turni non ancora assegnati!"), new GridBagConstraints());
			}
		}

		this.add(mainPanel);

	}

}
