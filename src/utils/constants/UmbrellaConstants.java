package utils.constants;

/**
 * 
 * @author Samuele Medici, samuele.medici2@studio.unibo.it ( Mat. 0000718877 )
 *
 */
public final class UmbrellaConstants {
	
	// Sizes
	public static final String LARGE = "L";
	public static final String MEDIUM = "M";
	public static final String SMALL = "S";
	
	// Path dell'icona
	public static final String UMBRELLA_ICON = "/umbrella.png";

}
