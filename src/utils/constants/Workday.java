package utils.constants;

public enum Workday {
	MONDAY, 
	TUESDAY, 
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
	SUNDAY
}